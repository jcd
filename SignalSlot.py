#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

# Copyright ® 2008 Fulvio Satta
#
# If you want contact me, send an email to Yota_VGA@users.sf.net
#
# This file is part of jcd
#
# jcd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# jcd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#TODO: Make the default implementation warning-based
#TODO: Test, test, test
#TODO: Polishing

##########################
##### IMPORT SECTION #####
##########################

from inspect     import getargspec  as _getargspec

from threading   import Lock        as _Lock

from Syncronized import Syncronized as _Syncronized
from Syncronized import AutoLock    as _AutoLock

#########################################
##### EXCEPTIONS DEFINITION SECTION #####
#########################################

#Exception always ignored in the exception handling
class SlotIgnore(Exception):
    pass

############################################
##### PUBLIC SUPPORT FUNCTIONS SECTION #####
############################################

#See if a prototype function match with the arguments
def matchingFunction(f, args, kwargs):
    #specs for call the function
    specs = _getargspec(f)

    #normalize specs[3]
    defp = specs[3]
    if not specs[3]:
        defp = ()

    #Get some usefull parameter for later
    min_params = len(specs[0]) - len(defp)
    poskey_params = min(len(specs[0]), len(args))

    #This have True for the declared positional elements, and false for the other positional key elements
    positional_params = [True] * poskey_params + [False] * (len(specs[0]) - poskey_params)

    #Counter for the key params contained in positional key params
    c = 0
    #Enumerate the positional key params
    for i, p in enumerate(specs[0]):
        #For each key param contained in the positional params
        if p in kwargs:
            #If is already declared continue to the next function
            if positional_params[i]:
                return False
            #Count it
            c += 1

    #If the function have too many positional parameters continue to the next function
    if not specs[1] and len(args) > len(specs[0]):
        return False

    #If the function have unknown keywords parameters continue to the next function
    if not specs[2] and len(kwargs) != c:
        return False

    return True

#Make a delayed callable from a SlotBase derived function.
#The function will be called immediately if possible, elsewere will be queued
def delayCaller(function):
    def call(*args, **kwargs):
        if not matchingFunction(function, args, kwargs):
            raise SlotIgnore

        self = function.im_self
        secondLock = False
        
        try:
            for i in xrange(2):
                if self.lock(blocking = False):
                    try:
                        function(self, *args, **kwargs)
                    finally:
                        self.unlock()
                    return
                if not secondLock:
                    secondLock = True
                    self.slotList.lock()
            self.slotList += function
        finally:
            if secondLock:
                self.slotList.unlock()

    return call()

############################################
##### SIGNAL/SLOT BASE CLASSES SECTION #####
############################################

#Class that one must be inherit for send signals
class SignalBase(_Syncronized):
    #Init the class
    def __init__(self):
        _Syncronized.__init__(self)

        self.slots = {}
        self.setupSignals()

    #Init the signals
    def setupSignals(self):
        pass

    #Add a signal
    @_AutoLock
    def addSignal(self, signal):
        if signal in self.slots:
            raise ValueError

        self.slots[signal] = []

    #Remove a signal
    @_AutoLock
    def removeSignal(self, signal):
        try:
            del self.slots[signal]
        except KeyError:
            raise ValueError

    #Emit a function
    @_AutoLock
    def emitOne(self, signal, function, args, kwargs):
        #If the function don't match skip to the next
        f = delayCaller(function)
        if not matchingFunction(f, args, kwargs):
            return

        #Try the function call and append the result in r
        try:
            r.append(f(*args, **kwargs))

        #If the function send SlotIgnore ignore it
        except SlotIgnore:
            pass

    #For exception management
    def emitOneProxy(self, signal, function, n, args, kwargs):
        try:
            self.emitOne(signal, function, args, kwargs)
        except:
            pass

    #Try all the functions (slots) associated with a signal
    @_AutoLock
    def emit(self, signal, *args, **kwargs):
        #If the signal don't exist send an exception
        try:
            l = self.slots[signal]
        except KeyError:
            raise ValueError

        #For each function
        for n, f in enumerate(l):
            self.emitOneProxy(signal, f, n, args, kwargs)

#Class that one must be inherit for receive signals
class SlotBase(_Syncronized):
    def __init__(self):
        #Support class for the list of slots
        class slotListClass(list, _Syncronized):
            def __init__(self):
                list.__init__(self)
                _Syncronized.__init__(self)
                self.__mutex = _Lock()

        _Syncronized.__init__(self)
        self.slotList = slotListClass()
        self.__locknums = 0

    #Rederinition for __locknums
    def lock(self, blocking = 1):
        r = _Syncronized.lock(self, blocking)
        self.__locknums += 1
        return r

    #For exception management
    def proxyUnlock(self, f, args, kwargs):
        try:
            f(self, *args, **kwargs)
        except:
            pass

    #Unlock the class executing the waiting slots
    def unlock(self):
        slotlock = False
        self.__locknums -= 1
        try:
            if not self.__locknums:
                self.slotList.lock()
                slotLock = True

                for f, args, kwargs in self.slotList:
                    self.proxyUnlock(f, self, *args, **kwargs)

        finally:
            _Syncronized.unlock(self)
            self.slotList.unlock()

#Class that generate and capture signals
class SignalSlotBase(SignalBase, SlotBase):
    def __init__(self):
        import PluginLoader

        SignalBase.__init__(self)
        SlotBase.__init__(self)
        PluginLoader.Loader().registerObject(self)

    #A slot for regen the signals
    @_AutoLock
    def renewSlots(self):
        self.slots = {}
        self.setupSignals()

    #A slot called when the plugins are reloaded
    @_AutoLock
    def reloadPlugins(self):
        self.renewSlots()

########################
##### TEST SECTION #####
########################

if __name__ == '__main__':
    class Try1(SignalSlotBase):
        def setupSignals(self):
            self.addSignal('ba')

    class Try2(SlotBase):
        def fun1(a, b):
            print '1'

        def fun2(a, b, c = None):
            print '2'

    t1 = Try1()
    t2 = Try2()
    t1.slots['ba'] += [t2.fun1, t2.fun2]

    t1.emit('ba', 1, b = 2)
