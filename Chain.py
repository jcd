#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

# Copyright ® 2008 Fulvio Satta
#
# If you want contact me, send an email to Yota_VGA@users.sf.net
#
# This file is part of jcd
#
# jcd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# jcd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#TODO: Test, test, test
#TODO: Polishing

##########################
##### IMPORT SECTION #####
##########################

from Syncronized import Syncronized as _Syncronized
from Syncronized import AutoLock    as _AutoLock

import sys as _sys

####################################
##### DEFAULT JUNCTORS SECTION #####
####################################

#Input junctor for no args reinterpretation
def inputDirectJunctor(args, kwargs):
    return (args, kwargs)

#Output junctor for no return reinterpretation
def outputDirectJunctor(ret, args, kwargs):
    return ([ret], {})

################################
##### MAIN CLASSES SECTION #####
################################

#A node of the chain
class Node(_Syncronized):
    def __init__(self, inputJunctor = inputDirectJunctor, outputJunctor = outputDirectJunctor):
        _Syncronized.__init__(self)

        self.inputJunctor = inputJunctor
        self.outputJunctor = outputJunctor

    #Call the node
    @_AutoLock
    def __call__(self, *args, **kwargs):
        rargs, rkwargs = self.inputJunctor(args, kwargs)
        return self.outputJunctor(self.work(*rargs, **rkwargs), rargs, rkwargs)

    #The work of the node, you must reimplement this
    def work(self, *args, **kwargs):
        print >> _sys.stderr, 'You must reimplement the work method in the Chain.Node inherited classes!'
        raise NotImplemented, 'You must reimplement the work method in the Chain.Node inherited classes!'

#The chain (nodes must be callable, but not strictly Node classes)
class Chain(_Syncronized):
    def __init__(self):
        _Syncronized.__init__(self)
        self.nodes = []

    #Call the chain
    @_AutoLock
    def __call__(self, *args, **kwargs):
        rargs, rkwargs = args, kwargs

        #Recursively work with the nodes
        for node in self.nodes:
            rargs, rkwargs = node(*rargs, **rkwargs)

        return rargs, rkwargs

########################
##### TEST SECTION #####
########################

if __name__ == '__main__':
    class Try1(Node):
        def work(self, var):
            return var + 1

    t1 = Chain()
    t1.nodes += [Try1(), Try1()]

    import inspect
    print inspect.getargspec(Try1().work)

    print t1(1)[0][0]
