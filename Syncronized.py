#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

# Copyright ® 2008 Fulvio Satta
#
# If you want contact me, send an email to Yota_VGA@users.sf.net
#
# This file is part of jcd
#
# jcd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# jcd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#TODO: Test, test, test
#TODO: Polishing

##########################
##### IMPORT SECTION #####
##########################

from threading import RLock         as _RLock
from threading import Condition     as _Condition
from threading import currentThread as _CurrentThread

from inspect   import isfunction    as _isfunction

################################
##### BASE CLASSES SECTION #####
################################

#Class usefull for sincronizzation purposes
class Syncronized:
    def __init__(self):
        self.__mutex = _RLock()

    #Locking mutex
    def lock(self, blocking = 1):
        return self.__mutex.acquire(blocking)

    #Unlocking mutex
    def unlock(self):
        return self.__mutex.release()

#Class usefull for sincronizzation purposes with rw locks
class RWSyncronized:
    def __init__(self):
        self.__status      = _Condition()
        self.__lock        = _RLock()
        self.__readers     = 0
        self.__writers     = False
        self.__usedThreads = {}

    def __addToThread(self):
        thread = _CurrentThread()
        original = self.__usedThreads.get(thread, 0)
        self.__usedThreads[thread] = original + 1
        return original

    #Locking for read
    def readLock(self, wait = None):
        self.__status.lock()
        try:
            if self.__addToThread():
                return True

            while True:
                if self.__writers:
                    self.__status.wait(wait)
                    continue

                r = True
                if not self.__readers:
                    r = self.__lock.acquire()

                self.__readers += 1
                return r
        finally:
            self.__status.release()

    #Locking for write
    def writeLock(self, blocking = 1):
        self.__status.acquire()
        try:
            self.__addToThread()
            self.__writers = True
            return self.__lock.acquire(blocking)
        finally:
            self.__status.release()

    #Unlocking the rwlock
    def rwunlock(self):
        self.__status.acquire()
        try:
            thread = _CurrentThread()
            self.__usedThreads[thread] -= 1
            if self.__usedThreads[thread]:
                del self.__usedThreads[thread]

            if self.__readers:
                self.__readers -= 1
            if not self.__readers:
                self.__lock.release()
        finally:
            self.__status.release()

##############################
##### DECORATORS SECTION #####
##############################

#Decorator for function autolock in the classes inerithed from Syncronized
def AutoLock(param):
    from types import FunctionType

    def member(self, *args, **kwargs):
        self.lock()
        try:
            return param(self, *args, **kwargs)
        finally:
            self.unlock()

    def function(f):
        def decorator(*args, **kwargs):
            if hasattr(param, 'lock'):
                param.lock()
            else:
                param.acquire()
            try:
                return f(*args, **kwargs)
            finally:
                if hasattr(param, 'unlock'):
                    param.unlock()
                else:
                    param.release()
        return decorator

    if type(param) == FunctionType:
        return member
    return function

#Decorator for function autolock in the classes inerithed from RWSyncronized
def RWAutoLock(param, write = False):
    from types import FunctionType

    def member(self, *args, **kwargs):
        if write:
            self.writeLock()
        else:
            self.readLock()
        try:
            return param(self, *args, **kwargs)
        finally:
            self.rwunlock()

    def function(f):
        def decorator(*args, **kwargs):
            if write:
                param.writeLock()
            else:
                param.readLock()
            try:
                return f(*args, **kwargs)
            finally:
                param.rwunlock()
        return decorator

    if type(param) == FunctionType:
        return member
    return function

########################
##### TEST SECTION #####
########################

if __name__ == '__main__':
    class Try1(Syncronized):
        pass

    t1 = Try1()
    t1.lock()
    t1.lock()
    t1.unlock()
    t1.unlock()
