#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

# Copyright ® 2008 Fulvio Satta
#
# If you want contact me, send an email to Yota_VGA@users.sf.net
#
# This file is part of jcd
#
# jcd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# jcd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#TODO: Exception improvements (for an i18n and many other automatic tasks)
#TODO: Test, test, test
#TODO: Polishing

##########################
##### IMPORT SECTION #####
##########################

import stringprep     as _stringprep  #Tables for stringprep

import encodings.idna as _idna        #Stringprep for domains

import unicodedata    as _unicodedata #For string normalizzation

#################################################
##### INTERNAL STRINGPREP FUNCTIONS SECTION #####
#################################################

#Character mapping for nodes
def _mapNode(c):
    if _stringprep.in_table_b1(c):
        return ''

    return _stringprep.map_table_b2(c)

#Character mapping for resources
def _mapResource(c):
    if _stringprep.in_table_b1(c):
        return ''

    return c

#Raise an exception if a character is proibited in a node name
def _proibitionNode(c):
    proibited_characters = [u'"', u'&', u"'", u'/', u':', u'<', u'>', u'@']

    if _stringprep.in_table_c11(c) or c in proibited_characters:
            raise UnicodeError('Invalid character (%c)' % c)

    return _proibitionResource(c)

#Raise an exception if a character is proibited in a resource name
def _proibitionResource(c):
    if  _stringprep.in_table_c12(c) or \
        _stringprep.in_table_c21(c) or \
        _stringprep.in_table_c22(c) or \
        _stringprep.in_table_c3(c)  or \
        _stringprep.in_table_c4(c)  or \
        _stringprep.in_table_c5(c)  or \
        _stringprep.in_table_c6(c)  or \
        _stringprep.in_table_c7(c)  or \
        _stringprep.in_table_c8(c)  or \
        _stringprep.in_table_c9(c):
            raise UnicodeError('Invalid character (%c)' % c)

    return c

#Unassigned characters (illegals)
def _unassigned(c):
    if _stringprep.in_table_a1(c):
        raise UnicodeError('Unassigned character (%c)' % c)

    return c

#Check if the string is correct for that concern the bidirectionality
#and that not contain some illecal character combination
def _checkBidirectional(s):
    NOBIDI = 0 #no bidirectional characters
    RAND   = 1 #Rand character
    LCAT   = 2 #LCat character

    type = NOBIDI #One of the constant NOBIDI, RAND or LCAT

    #Some state variables
    first     = True
    firstRand = False
    lastRand  = False

    #for each character
    for c in s:
        #Rand character
        if _stringprep.in_table_d1(c):
            if first:
                firstRand = True

            #Rand and LCat character togheter
            if type == LCAT:
                raise UnicodeError('Bidirectional characters from ' \
                        'both of the stringprep bidirectional tables')

            #If the string have some Rand character the first character must
            #be a Rand character
            if not firstRand:
                raise UnicodeError('Rand character whitout initial ' \
                        'rand character')

            type = RAND
            lastRand = True

        #Non rand character
        else:
            lastRand = False

            #LCat character
            if _stringprep.in_table_d2(c):
                #Rand and LCat character togheter
                if type == RAND:
                    raise UnicodeError('Bidirectional characters ' \
                            'from both of the stringprep ' \
                            'bidirectional tables')

                type = LCAT

        first = False

    #If the string have some Rand character the last character must be a Rand
    #character
    if type == RAND and not lastRand:
        raise UnicodeError('Rand character whitout final ' \
                'rand character')

#################################################
##### EXTERNAL STRINGPREP FUNCTIONS SECTION #####
#################################################

#Check a string with nodeprep
def nodeprep(s):
    np = _unicodedata.normalize('NFKC', u''.join(map(_mapNode, s)))
    np = map(_proibitionNode, np)

    map(_unassigned, np)

    _checkBidirectional(np)
    return ''.join(np)

#Check a string with resourceprer
def resourceprep(s):
    rp = _unicodedata.normalize('NFKC', u''.join(map(_mapResource, s)))
    rp = map(_proibitionResource, rp)

    map(_unassigned, rp)

    _checkBidirectional(rp)
    return ''.join(rp)

#############################
##### JID CLASS SECTION #####
#############################

#JID class conformed to the RFCs
class JID:
    def __init__(self, jid = None, node = None, domain = None, \
            resource = None, isNormalized = False):
        #If is an istance of JID skip the normal assign (for prestational
        #purposes)
        if isinstance(jid, JID):
            #The JID is normalized
            self.__isNormalized = True

            #Copy the variables
            self.node     = jid.node
            self.domain   = jid.domain
            self.resource = jid.resource

            #For the future the set will be normalized
            self.__isNormalized = False
            return

        #All the strings are automatically casted to unicode
        if node     != None: node     = unicode(node)
        if domain   != None: domain   = unicode(domain)
        if resource != None: resource = unicode(resource)

        #If the user send a jid like a string split that
        if jid:
            j = unicode(jid)
            if j.find('@') >= 0:
                node, j = j.split('@', 1)

            if j.find('/') >= 0:
                j, resource = j.split('/', 1)

            domain = j

        #Set if the JID is normalized
        self.__isNormalized = isNormalized

        #Set the JID variables
        self.node = node
        self.domain = domain
        self.resource = resource

        #For the future the set will be normalized
        self.__isNormalized = False

    #Reinterpreter for node, domain and resource attributes
    def __setattr__(self, item, value):
        #node attribute
        if item == 'node':
            self.__dict__['node'] = node = value

            if not self.__isNormalized and node:
                node = nodeprep(value)
            self.__nnode = node

            if self.__nnode and len(self.__nnode) > 1023:
                raise ValueError('Node too long')

        #domain attribute
        elif item == 'domain':
            self.__dict__['domain'] = domain = value

            if not self.__isNormalized and domain:
                domain = _idna.ToASCII(_idna.nameprep(domain)).lower()
            self.__ndomain = domain

            if not domain:
                raise ValueError('JID must have a domain')

            if len(self.__ndomain) > 1023:
                raise ValueError('Domain too long')
        
        #resource attribute
        elif item == 'resource':
            self.__dict__['resource'] = resource = value

            if not self.__isNormalized and resource:
                resource = resourceprep(resource)
            self.__nresource = resource

            if self.__nresource and len(self.__nresource) > 1023:
                raise ValueError('Resource too long')

        #default action
        else:
            self.__dict__[item] = value

    #Obtain the unicode rappresentation of a jid
    def __str__(self):
        #Only the domain in jid
        jid = self.domain

        #Add the node part
        if self.node != None:
            jid = self.node + u'@' + jid

        #Add the resource part
        if self.resource != None:
            jid += u'/' + self.resource

        return jid

    #Equivalence operator
    def __eq__(self, other):
        b = JID(other)

        return self.__nnode     == b.__nnode     and \
               self.__ndomain   == b.__ndomain   and \
               self.__nresource == b.__nresource

    #Unequivalence operator
    def __ne__(self, other):
        return not (self == other)

    #Hash generation
    def __hash__(self):
        return hash(unicode(self.normalized()))

    #Give the normalized corresponding JID
    def normalized(self):
        return JID(node = self.__nnode, domain = self.__ndomain, \
                resource = self.__nresource, isNormalized = True)

    #Get the bare JID
    def bareJID(self):
        jid = JID(node = self.node, domain = self.domain, \
                isNormalized = True)

        jid.__nnode = self.__nnode
        jid.__ndomain = self.__ndomain
        return jid

##########################################
##### XEP 106 (JID ESCAPING) SECTION #####
##########################################

#Escape codes of all the possible escapes excpet that for the \ character
_escape_codes = [('20', ' '), \
                 ('22', '"'), \
                 ('26', '&'), \
                 ('27', "'"), \
                 ('2f', '/'), \
                 ('3a', ':'), \
                 ('3c', '<'), \
                 ('3e', '>'), \
                 ('40', '@')]

#XEP 106: Escape a node
def escapeJIDNode(node, stripSpaces = True):
    #Strip all the spaces in the jid if required
    if stripSpaces:
        node = node.strip(' ')

    #If some space is at the start or the end of a JID raise an exception
    if node[0] == ' ' or node[-1] == ' ':
        raise ValueError('Spaces first or last in the JID')

    s = node
    for code, character in _escape_codes:
        #Replace all the codes
        s = s.replace('\\' + code, r'\5c' + code)
        s = s.replace(character, '\\' + code)

    return JID(s)

#XEP 106: Unescape a node
def unescapeJIDNode(node, stripSpaces = True):
    #Strip all the spaces in the jid if required
    if stripSpaces:
        node = node.strip(' ')

    #If some space is at the start or the end of a JID raise an exception
    if node[0] == ' ' or node[-1] == ' ':
        raise ValueError('Spaces first or last in the JID')

    #If the node start or end with \20 return the unaltered node
    if s.startswith(r'\20') or s.endswith(r'\20'):
        return node

    #Replace the codes eccept that \
    for code, character in _escape_codes:
        s = s.replace('\\' + code, character)

    #Replace the code for \
    s = s.replace(r'\5c', '\\')

    return s

########################
##### TEST SECTION #####
########################

if __name__ == '__main__':
    a = JID('a@b/c')
    b = JID(a)

    print a == 'A@b/c'
    print escapeJIDNode(' ba@')
    print hash(a)
    print a.bareJID()
