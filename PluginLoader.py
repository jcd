#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: expandtab:shiftwidth=4:fileencoding=utf-8 :

# Copyright ® 2008 Fulvio Satta
#
# If you want contact me, send an email to Yota_VGA@users.sf.net
#
# This file is part of jcd
#
# jcd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# jcd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#TODO: Test, test, test
#TODO: Polishing
#TODO: Comment better
#TODO: Complete the syncronization part
#TODO: Manage the plugin unloading correctly
#TODO: Complete the reloading part

##########################
##### IMPORT SECTION #####
##########################

import warnings as _warnings

from Syncronized import AutoLock      as _AutoLock
from Syncronized import RWAutoLock    as _RWAutoLock
from Syncronized import RWSyncronized as _RWSyncronized

from SignalSlot  import SignalBase    as _SignalBase

from threading   import RLock         as _RLock

#####################################
##### EXTERNAL VARIABLE SECTION #####
#####################################

#Plugin list
plugins = []

#Inited plugin list
loaded = []

#Directorys checked
dir_list = []

#Module mutex
mutex = _RLock()

#User blacks
user_blacks = []

#Auto blacks
auto_blacks = []

######################################
##### INTERNAL FUNCTIONS SECTION #####
######################################

#Import a plugin
def _import(plugin, dir, plugname):
    import imp
    import os
    import sys

    #Get the paths
    fullpath = os.path.join(dir, plugin.replace(u'.', os.sep))
    path, name = os.path.split(fullpath)

    #If the plugin is already exported return it
    try:
        return sys.modules[plugname]
    except KeyError:
        pass

    #Find the module
    fp, pathname, description = imp.find_module(name, [path])

    #Import the module
    try:
        return imp.load_module(plugname, fp, pathname, description)
    finally:
        if fp:
            fp.close()

#Load a plugin, but not init that
def _load(plugin, dir):
    from codecs import open

    lines = open(plugin, encoding = 'utf8').readlines()
    lines = [line.strip() for line in lines if line[0] != u'#' and line.strip()]

    try:
        module = _import(lines[0], dir, plugin)
    except Exception, e:
        _warnings.warn('Plugin %s have an import error: %s, %s, %s' % (
                                                    os.path.join(dir, plugin),
                                                    type(e), e, e.args),
                                                ImportWarning, stacklevel = 2)
    except:
        _warnings.warn('Plugin %s have an import error' %
                                                    os.path.join(dir, plugin),
                                                ImportWarning, stacklevel = 2)
    else:
        return module

    return None

######################################
##### EXTERNAL FUNCTIONS SECTION #####
######################################

#Auboblack the modules
@_AutoLock(mutex)
def filterBlacks(plugins):
    import os

    refilter = False
    new_plugins = []
    black_plugins = []
    new_black_plugins = []

    for plugin in plugins:
        noblack = True
        try:
            black = hasattr(plugin[0], 'loadable') and not plugin.loadable(plugin[0])
        except Exception, e:
            _warnings.warn('Plugin %s have a loadable error: %s, %s, %s' % (
                                                        plugin[0].__name__,
                                                        type(e), e, e.args),
                                                    ImportWarning, stacklevel = 2)
        except:
            _warnings.warn('Plugin %s have a loadable error' %
                                                        plugin[0].__name__,
                                                    ImportWarning, stacklevel = 2)

        if noblack:
            new_plugins.append(plugin)
        else:
            black_plugins.append(plugin)
            refilter = True

    if refilter:
        new_plugins, new_black_plugins = filterBlacks(new_plugins)

    return new_plugins, black_plugins + new_black_plugins

#Find the plugins.
@_AutoLock(mutex)
def findPlugins(dirs, numbers, order, blackList = []):
    from glob import glob
    import os

    #Default values
    modules = {}
    modules_names = {}

    #For each dir
    for dir in dirs:
        #Get the plugins in the directory
        dir = os.path.abspath(os.path.expanduser(dir))
        plugins = glob(os.path.join(dir, '*.plug'))

        #For each plugin
        for plugin in plugins:
            #If the plugin is in the black list go to the next
            if plugin in blackList:
                user_blacks.append(plugin)
                continue

            #Try to load the module
            module = _load(plugin, dir)
            if not module:
                continue

            #Get the priority number
            number = 0
            try:
                number = module.priority
            except AttributeError:
                pass
            try:
                number = numbers[plugin]
            except KeyError:
                pass

            #If an order is defined follow the order, else append the module
            #as the last of the list
            try:
                index = order.index(plugin)
            except ValueError:
                modules[number] = modules.get(number, []) + [module]
            else:
                actuals = modules_names.get(number, [])
                modules_actuals = modules.get(number, [])
                n = 0
                for name in order[:index]:
                    if name in actuals:
                        n += 1
                modules_actuals.insert(n, module)
                actuals.insert(n, plugin)
                modules[number] = modules_actuals
                modules_names[number] = actuals

    return modules

#Flat the modules in a list
def flattingModules(modules):
    r = []

    numbers = modules.keys()
    numbers.sort()

    for number in numbers:
        r += zip(modules[number], [number] * len(modules[number]))

    return r

#Remove the plugins in remove but not in mantain from sys.modules
@_AutoLock(mutex)
def removePlugins(mantain, remove):
    import sys

    for i in remove:
        if module not in mantain:
            try:
                del sys.modules[module.__name__]
            except KeyError:
                pass

#Set the directory lists and load the plugins
@_AutoLock(mutex)
def setDirs(dirs, pluginOrderingList, blackList = [], autoremotion = True):
    global plugins, dir_list, numbered_plugins, auto_blacks

    numbers = dict(pluginOrderingList)
    order = [i[0] for i in pluginOrderingList]
    old_plugins = plugins + auto_blacks

    dir_list = dirs
    numbered = findPlugins(dirs, numbers, order)
    plugins = flattingModules(numbered)

    if autoremotion:
        removePlugins(plugins, old_plugins)

    plugins, auto_blacks = filterBlacks(plugins)

    return [plugin for plugin in old_plugins if plugin not in plugins]

#Select a group of plugins
@_AutoLock(mutex)
def getPlugins(selector, mantainNumbers = False):
    if mantainNumbers:
        return [plugin for plugin in plugins if selector(plugin)]
    return [plugin[0] for plugin in plugins if selector(plugin[0])]

#Reload a plugin
#WARNING: Very low level function! You must know what you do!
@_AutoLock(mutex)
def reload(plugin):
    import os
    import imp

    #The original settings
    fullpath = plugin.__file__
    path, name = os.path.split(fullpath)
    name = name.rsplit('.', 1)[0]

    #Search the module
    fp, pathname, description = imp.find_module(name, [path])

    #Reimport the module
    try:
        return imp.load_module(plugin.__name__, fp, pathname, description)
    finally:
        if fp:
            fp.close()

##################################
##### PLUGIN CLASSES SECTION #####
##################################

class Loader:
    class __implementation(_RWSyncronized, _SignalBase):
        def __init__(self):
            _RWSyncronized.__init__(self)
            _SignalBase.__init__(self)

        def setupSignals(self):
            self.addSignal('reloadPlugins')

    __imp = __implementation()

    @_AutoLock(__imp)
    def registerObject(self, object):
        self.__imp.slots['reloadPlugins'].append(object.reloadPlugins)

    #Init a plugin
    @_AutoLock(mutex)
    def loadPlugin(self, plugin):
        if plugin not in loaded:
            if hasattr(plugin, 'init'):
                plugin.init()
            loaded.append(plugin)

    #Init many plugins
    def loadPlugins(self, plugins):
        for plugin in plugins:
            self.loadPlugin(plugin)

    #Sett all the plugins as unloadables
    @_AutoLock(mutex)
    @_RWAutoLock(__imp, write = True)
    def startReloading(self):
        global loaded
        self.__imp.oldplugins = loaded
        self.__imp.emit('reloadPlugins')
        loaded = []

    #Unload all the plugins non reloaded
    @_AutoLock(mutex)
    @_RWAutoLock(__imp, write = True)
    def stopReloading(self):
        for plugin in self.__imp.oldplugins:
            if plugin not in loaded and hasattr(plugin, 'deinit'):
                plugin.deinit()

    #Lock plugin unloading
    def lock(self, wait = None):
        mutex.lock()
        try:
            return self.__imp.readLock(wait)
        except:
            mutex.unlock()

    #unlock plugin unloading
    def unlock(self):
        self.__imp.writeLock()
        mutex.unlock()

##############################
##### DECORATORS SECTION #####
##############################

#Decorator for Loader sincronization
def LockPluginUnloading(f):
    loader = Loader()

    def function(*args, **kwargs):
        loader.lock()
        try:
            return f(*args, **kwargs)
        finally:
            loader.unlock()

    return function

########################
##### TEST SECTION #####
########################

if __name__ == '__main__':
    order = {1 : ('plugins/example.plug')}

    setDirs(['plugins'], flattingModules(order))

    plugin = getPlugins(lambda plugin: plugin.task['name'] == 'ExampleTask')[0]
    loader = Loader()
    loader.loadPlugin(plugin)
    loader.loadPlugin(plugin)

    loader.startReloading()
    loader.loadPlugin(plugin)
    loader.stopReloading()

    print plugins
